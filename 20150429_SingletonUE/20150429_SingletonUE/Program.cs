﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20150429_SingletonUE
{
    class Program
    {
        static void Main(string[] args)
        {
            Direktor haidl = Direktor.getInstance();
        }
    }
    public class Direktor
    {
        public static Direktor Haidl;
        private Direktor() { }
        public static Direktor getInstance()
        {
            if(Haidl == null)
            {
                Haidl = new Direktor();
            }
            return Haidl;
        }
    }
}
