﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodBsp
{
    class Program
    {
        static void Main(string[] args)
        {
            Pizzeria daToni = new Pizzeria();
            daToni.Mahlzeitliefern();
            Console.WriteLine("_______");
            Würstelbude Würstelsepp = new Würstelbude();
            Würstelsepp.Mahlzeitliefern();
            Console.WriteLine("_______");
            Kaffeehaus Wagner = new Kaffeehaus();
            Wagner.Mahlzeitliefern();
        }
    }
}
