﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodBsp
{
    //Erzeuger
    abstract class Restaurant
    {
        protected Mahlzeit mahlzeit;

        protected virtual void MahlzeitZubereiten()
        { }
        protected virtual void BestellungAufnehmen()
        {
            Console.WriteLine("Ihre Bestellung bitte");
        }
        protected virtual void MahlzeitServieren()
        {
            Console.WriteLine("Mahlzeit fertig");
        }

        //öffentliche Factorymethode
        public void Mahlzeitliefern()
        {
            BestellungAufnehmen();
            MahlzeitZubereiten();
            MahlzeitServieren();
        }
    }
    class Pizzeria : Restaurant
    {
        protected override void MahlzeitZubereiten()
        {
            mahlzeit = new Pizza();
        }
    }
    class Würstelbude : Restaurant
    {
        protected override void BestellungAufnehmen()
        {
            Console.WriteLine("S'wüst'n?");
        }
        protected override void MahlzeitZubereiten()
        {
            mahlzeit = new Bratwurst("Semmel");
        }
    }
    class Kaffeehaus : Restaurant
    {
        protected override void MahlzeitServieren()
        {
            Console.WriteLine("Guten Appetit");
        }
        protected override void BestellungAufnehmen()
        {
            Console.WriteLine("Wie können wir Ihnen dienen?");
        }
        protected override void MahlzeitZubereiten()
        {
            mahlzeit = new Torte();
        }
    }
}
