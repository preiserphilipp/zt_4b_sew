﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodBsp
{
    //Produkt
    class Mahlzeit
    {
    }
    class Pizza : Mahlzeit
    {
        public Pizza()
        {
            Console.WriteLine("Pizza gebacken");
        }
    }
    class Bratwurst : Mahlzeit
    {
        public Bratwurst(string Beilage)
        {
            Console.WriteLine("Wurst Gestopft, mit "+Beilage);
        }
    }
    class Torte : Mahlzeit
    {
        public Torte()
        {
            Console.WriteLine("Torte getortet");
        }
    }
}
