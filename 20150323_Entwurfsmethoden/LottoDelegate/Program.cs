﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoDelegate
{
    class Program
    {
        static void Main(string[] args)
        {
            LottoServer ls = new LottoServer();
            ls.TellMe += ergebnis;
            ls.Ziehung();
        }

        static void ergebnis(string antwort)
        {
            Console.WriteLine(antwort);
        }
    }
}
