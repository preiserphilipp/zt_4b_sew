﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoDelegate
{
    class LottoServer
    {
        Random r = new Random();
        public delegate void Update(string message);
        public event Update TellMe;
        public void Ziehung()
        {
            string tmp = "";
            for (int i = 0; i < 6; i++)
            { //ok doppelte gehören auch geprüft
                if (i > 0)
                    tmp += "-";
                tmp += r.Next(1, 45);
            }
            if (TellMe != null)
                TellMe(tmp);
        }
        public class LottoObserver
        {
 
        }
    }
}
