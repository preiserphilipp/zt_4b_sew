﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Einzelstück;

namespace Entwurfsmustertester
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Singleton s1 = Singleton.getInstance();
            Singleton s2 = Singleton.getInstance();
            Assert.AreEqual(s1, s2);
        }
    }
}
