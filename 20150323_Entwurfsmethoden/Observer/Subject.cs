﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public class Subject
    {
        List<Observer> meineZuhoerer = new List<Observer>();
        protected string myState; //abgeleitete Klasse verändert die

        public void Broadcast()
        {
            foreach (Observer item in meineZuhoerer)
            {
                item.Update(myState); //benachrichtigt alle Zuhörer
            }
        }
        public void Login(Observer neu)
        {
            meineZuhoerer.Add(neu);
        }
        public void Logout(Observer alt)
        {
            meineZuhoerer.Remove(alt);
        }
        //public string GetState()
        //{
        //    return myState;
        //}
    }

    public class Witzeerzähler : Subject
    {
        //muss sich Witze merken
        List<string> meineWitze = new List<string>()
        {
            "Witz 1", "Witz2", "Witz3"
        };
        Random r = new Random();
        public void ErzähleEinenWitz()
        {
            myState=meineWitze[r.Next(0, meineWitze.Count)];
            Broadcast(); //allen Zuörern den Witz erzählen
        }
    }
    public class LottoServer : Subject
    {
        Random r = new Random();
        public void Ziehung()
        {
            myState = "";
            for (int i = 0; i < 6; i++)
            { //ok doppelte gehören auch geprüft
                if (i > 0)
                    myState += "-";
                myState += r.Next(1,45);
            }
            Broadcast();
        }
    }
}
