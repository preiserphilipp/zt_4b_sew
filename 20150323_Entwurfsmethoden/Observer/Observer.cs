﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public abstract class Observer
    {
        public abstract void Update(string vonwem);
    }
    public class WitzClient:Observer
    {
        public string Name { get; set; }
        public override void Update(string was)
        {
            Console.WriteLine(Name + ": Ich lach mich tot wegen: " + was);
        }
    }
    public class LottoClient : Observer {
        public string Name { get; set; }
        public override void Update(string was)
        {
            Console.WriteLine("Die Zahlen sind: "+ was);
        }
    }
}
