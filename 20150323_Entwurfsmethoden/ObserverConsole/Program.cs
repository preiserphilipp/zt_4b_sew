﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Observer;

namespace ObserverConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Witzeerzähler mundlSackbauer = new Witzeerzähler();
            LottoServer win2day = new LottoServer();

            WitzClient a = new WitzClient { Name = "Fleischi" };
            LottoClient b = new LottoClient { Name = "Oma" };

            mundlSackbauer.Login(a);
            win2day.Login(a);
            win2day.Login(b);
            mundlSackbauer.ErzähleEinenWitz();
            win2day.Ziehung();

            win2day.Logout(a);
            Console.WriteLine("--");
            win2day.Ziehung();
        }
        //ObserverDelegateEvent
        public static void TellJoke()
        {
            Console.WriteLine("Should tell a Joke");
        }
    }
}
