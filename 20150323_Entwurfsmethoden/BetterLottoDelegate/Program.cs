﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterLottoDelegate
{
    class Program
    {
        static void Main(string[] args)
        {
            ObservableLottoServer win2day = new ObservableLottoServer();
            win2day.SomethingHappened += HandleEvent;

            win2day.DoSomething();
        }
        static public void HandleEvent(object sender, EventArgs args)
        {
            Console.WriteLine("Zahlen: "+ (sender as ObservableLottoServer).Numbers);
        }
    }
    class ObservableLottoServer
    {
        public event EventHandler SomethingHappened;
        public string Numbers { get; private set; }
        Random r = new Random();
        public void DoSomething()
        {
            EventHandler handler = SomethingHappened;
            if (handler != null)
            {
                string tmp = "";
                for (int i = 0; i < 6; i++)
                { //ok doppelte gehören auch geprüft
                    if (i > 0)
                        tmp += "-";
                    tmp += r.Next(1, 45);
                }
                Numbers = tmp;
                handler(this, EventArgs.Empty);
            }
        }
    }
}
