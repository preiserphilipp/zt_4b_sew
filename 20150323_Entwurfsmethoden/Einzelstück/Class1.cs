﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Einzelstück
{
    public class Singleton
    {
        private static Singleton mySingleton;
        private Singleton() //privater Konstruktor tut vorerst nichts
        {
        }
        public static Singleton getInstance()
        {
            if(mySingleton==null) //gibt es eine Instanz?
                mySingleton = new Singleton(); //erzeuge Instanz von mir selber
            return mySingleton;
        }
    }
}
