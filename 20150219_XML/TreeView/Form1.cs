﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace TreeView
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TreeNode root = treeView.Nodes.Add("Desktop");     // 1. Knoten wird gesetzt
            root.Tag = @"C:\Users\Philipp\Desktop";
            MakeTree(@"C:\Users\Philipp\Desktop", root);
        }
        
        private void MakeTree(string path, TreeNode current)
        {
            FileInfo info = new FileInfo(path);
            DirectoryInfo di = new DirectoryInfo(path);

            foreach (var item in di.GetDirectories())
            {
                // Erzuege neen TreeNode
                TreeNode added = current.Nodes.Add(item.Name);
                added.Tag = item.FullName; // wird später benötigt

                // rekursion
                MakeTree(item.FullName, added);
            }

        }
        private void showFiles(string path, TreeNode current)
        {
            FileInfo info = new FileInfo(path);
            DirectoryInfo di = new DirectoryInfo(path);

            foreach (var item in di.GetFiles())
            {
                listView1.Items.Add(item.FullName);
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                listView1.Items.Clear();
                DirectoryInfo di = new DirectoryInfo(e.Node.Tag.ToString());

                foreach (var item in di.GetFiles())
                {
                    listView1.Items.Add(new ListViewItem(new[] { item.Name,             
                                                                 item.Length.ToString(),                            
                                                                 item.CreationTime.ToShortTimeString(),
                                                                 item.LastWriteTime.ToLongTimeString(),
                                                                 item.LastAccessTime.ToShortTimeString()
                                                            }));
                }
            }
            catch { }
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                listView1.Items.Clear();
                DirectoryInfo di = new DirectoryInfo(e.Node.Tag.ToString());

                foreach (var item in di.GetFiles())
                {
                    listView1.Items.Add(new ListViewItem(new[] { item.Name }));
                }
            }
            catch { }
        }

        static XmlDocument xd = new XmlDocument();
        private void export_Click(object sender, EventArgs e)
        {
            xd.LoadXml("<Desktop/>");
            XmlNode root = xd.SelectSingleNode("//Desktop");

            MakeXML(@"C:\Users\Philipp\Desktop", root);
            MessageBox.Show(xd.InnerXml); //Was steht im XML?
            xd.Save("test.xml");
        }
        static private void MakeXML(string path, XmlNode current)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo fi = new FileInfo(path);

            foreach (var item in di.GetDirectories())
            {
                XmlNode neu = xd.CreateNode("element", "DIR", "");
                current.AppendChild(neu);

                XmlAttribute name = xd.CreateAttribute("name");
                name.Value = item.Name;
                neu.Attributes.Append(name);

                MakeXML(item.FullName, neu);
            }
            foreach (var item in di.GetFiles())
            {
                XmlNode neu = xd.CreateNode("element", "FILE", "");
                current.AppendChild(neu);

                XmlAttribute name = xd.CreateAttribute("name");
                name.Value = item.Name;
                neu.Attributes.Append(name);

                //MakeXML(item.FullName, neu);
            }
        }
    }
}
