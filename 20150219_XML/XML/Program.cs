﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XML
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument x = new XmlDocument();
            x.LoadXml("<beispiel> <bla/> </beispiel>");

            XmlNode root = x.SelectSingleNode("//beispiel");    // XPath

            XmlNode neu = x.CreateNode("element", "neu", "");

            root.AppendChild(neu);

            x.Save("beispiel.xml");
        }
    }
}
