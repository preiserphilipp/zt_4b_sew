﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Xml_Tree
{
    class Program
    {
        static XmlDocument xd = new XmlDocument();

        static void Main(string[] args)
        {
            xd.LoadXml("<asdf/>");
            XmlNode root = xd.SelectSingleNode("//asdf");

            MakeXML(@"C:\Users\Philipp\Desktop", root);

            xd.Save("test.xml");
        }

        static private void MakeXML(string path, XmlNode current)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo fi = new FileInfo(path);

            foreach(var item in di.GetDirectories())
            {
                XmlNode neu = xd.CreateNode("element", "DIR", "");
                current.AppendChild(neu);

                XmlAttribute name = xd.CreateAttribute("name");
                name.Value = item.Name;
                neu.Attributes.Append(name);

                MakeXML(item.FullName, neu);
            } 
            foreach (var item in di.GetFiles())
            {
                XmlNode neu = xd.CreateNode("element", "FILE", "");
                current.AppendChild(neu);

                XmlAttribute name = xd.CreateAttribute("name");
                name.Value = item.Name;
                neu.Attributes.Append(name);

                //MakeXML(item.FullName, neu);
            }

        }
    }
}
