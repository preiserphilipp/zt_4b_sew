﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filesystem
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Users\Philipp\Desktop\asdf";
            //AusgabeFullName(path);
            Console.WriteLine(AusgabeName(path, 0)); // 0 da erste Ebene zu durchsuchen
        }

        static StringBuilder sb = new StringBuilder();

        static void AusgabeFullName(string path)
        {
            // gibt mit FullName den vollständigen Dateipfad an
            //Console.WriteLine(path);
            //DirectoryInfo di = new DirectoryInfo(path);
            //foreach (var item in di.GetDirectories())
            //{
            //    AusgabeFullName(item.FullName);
            //}     
        }

        static string AusgabeName(string path, int depth)
        {
            // gibt mit Name den unvollständigen Dateipfad - nur aktuellen Ordner - an
            for (int i = 1; i <= depth; i++)
                sb.Append("\t");

            sb.Append(new DirectoryInfo(path).Name + "\n");
            DirectoryInfo di = new DirectoryInfo(path);
           
            foreach (var item in di.GetDirectories())
            {
                AusgabeName(item.FullName, depth + 1);
            }

            return sb.ToString();
        }
    }
}
