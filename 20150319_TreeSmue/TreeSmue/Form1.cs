﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TreeSmue
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        string desktop = @"c:\Users\Philipp\Desktop\";

        private void button1_Click(object sender, EventArgs e)
        {
            TreeNode root = treeView1.Nodes[0];
            if (Directory.Exists(desktop + root.Text))
            {
                Directory.Delete(desktop + root.Text, true);
            }
            Directory.CreateDirectory(desktop + root.Text);

            MakeFolderRecursive(root);
        }

        private void MakeFolderRecursive(TreeNode curr)
        {
            // Lösungsidee: // nicht verpflichtend
            // Für jeden "Tree"-Unterknoten
            // ermittle seinen Fullpath
            // erzeuge ein Verzeichnis aus "desktop" + vollen Pfad des knotens
            foreach (TreeNode item in curr.Nodes)
            {
                string fullpath = curr.FullPath;
                Directory.CreateDirectory(desktop + fullpath);
                MakeFolderRecursive(item);// und wende die Rekursion an
            }
            

        }
    }
}
