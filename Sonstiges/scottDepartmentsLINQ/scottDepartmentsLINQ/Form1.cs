﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScottContext;

namespace scottDepartmentsLINQ
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ScottContext.ScottDataContext dc = new ScottDataContext();

        private void Form1_Load(object sender, EventArgs e)
        {
            var depts = from d in dc.Depts select d;

            foreach (var item in depts)
            {
                comboBox1.Items.Add(item.DEPTNO);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "") return;
            var currdep = from d in dc.Depts
                          where d.DEPTNO == Convert.ToInt32(comboBox1.SelectedItem.ToString())
                          select d;
            tbDname.Text = currdep.FirstOrDefault().DNAME;
            tbLoc.Text = currdep.FirstOrDefault().LOC;
            var curremp = from emp in dc.Emps
                             where emp.DEPTNO == Convert.ToInt32(comboBox1.SelectedItem.ToString())
                             select emp;
            flowLayoutPanel1.Controls.Clear();
            foreach (var item in curremp)
            {
                flowLayoutPanel1.Controls.Add(new Label() { Width=300, Text = item.ENAME + " " + item.JOB + " " + item.DEPTNO});
            }
        }
    }
}
