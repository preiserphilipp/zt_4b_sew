﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCAD_V1
{
    class MyGDIRectangle : MyRectangle, IDrawable
    {
        public MyGDIRectangle(int x, int y, int width, int height, string color, int thickness)
            : base (x, y, width, height, color, thickness)
        {

        }

        public void Draw(Graphics g)
        {
            g.DrawRectangle(new Pen(Color.FromName(color), thickness), x, y, width, height);
        }
        public bool selected(int mouseX, int mouseY)
        {
            if (x < mouseX && (x + width) > mouseX && y < mouseY && (y + height) > mouseY)
                return true;
            return false;
        }
        public void highlight(Graphics g, Form1 f)
        {
            int x2 = x + width;
            int y2 = y + height;
            g.DrawRectangle(new Pen(Color.Black, 3), x - 5, y - 5, 10, 10);
            g.DrawRectangle(new Pen(Color.Black, 3), x - 5, y2 - 5, 10, 10);
            g.DrawRectangle(new Pen(Color.Black, 3), x2 - 5, y2 - 5, 10, 10);
            g.DrawRectangle(new Pen(Color.Black, 3), x2 - 5, y - 5, 10, 10);

            f.tB_Rectangle_x1.Text = x.ToString();
            f.tB_Rectangle_y1.Text = y.ToString();
            f.tB_Rectangle_Width.Text = width.ToString();
            f.tB_Rectangle_Height.Text = height.ToString();

            f.tB_Thickness.Text = thickness.ToString();
            f.lB_Color.Text = color.ToString();
        }
        public override string ToString()
        {
            return "Rectangle";
        }
    }
}
