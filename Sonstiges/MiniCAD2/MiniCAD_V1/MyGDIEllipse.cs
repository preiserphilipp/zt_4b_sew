﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCAD_V1
{
    class MyGDIEllipse : MyEllipse, IDrawable
    {
        public MyGDIEllipse(int x, int y, int r1, int r2, string color, int thickness)
            : base (x, y, r1, r2, color, thickness)
        {

        }
        public bool selected(int mouseX, int mouseY) { return true; }
        public void highlight(Graphics g, Form1 f) {
            f.tB_Ellipse_x.Text = x.ToString();
            f.tB_Ellipse_y.Text = y.ToString();
            f.tB_Ellipse_r1.Text = r1.ToString();
            f.tB_Ellipse_r2.Text = r2.ToString();

            f.tB_Thickness.Text = thickness.ToString();
            f.lB_Color.Text = color.ToString();
        }
        public void Draw(Graphics g)
        {
            g.DrawEllipse(new Pen(Color.FromName(color), thickness), x, y, r1, r2);
        }

        public override string ToString()
        {
            return "Elippse";
        }
    }
}
