﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GraphObjects;
using System.Drawing.Drawing2D;

namespace MiniCAD_V1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        List<IDrawable> shapes = new List<IDrawable>();
        IDrawable selectedItem;

        private void Form1_Load(object sender, EventArgs e)
        {
            gB_Line.Enabled = false;
            gB_Circle.Enabled = false;
            gB_Ellipse.Enabled = false;
            gB_Rectangle.Enabled = false;
        }
        private void b_Line_Draw_Click(object sender, EventArgs e)
        {
            Graphics g = p_Draw.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if ((lB_Color.Text != "") && (tB_Thickness.Text != ""))
            {
                /*l = new MyGDILine(
                                      int.Parse(tB_Line_x1.Text),
                                      int.Parse(tB_Line_y1.Text),
                                      int.Parse(tB_Line_x2.Text),
                                      int.Parse(tB_Line_y2.Text),
                                      lB_Color.Text,
                                      int.Parse(tB_Thickness.Text)
                                      );*/
                shapes.Add(new MyGDICircle(int.Parse(tB_Line_x1.Text),
                                              int.Parse(tB_Line_y1.Text),
                                              int.Parse(tB_Line_x2.Text),
                                              int.Parse(tB_Line_y2.Text),
                                              lB_Color.Text,
                                              int.Parse(tB_Thickness.Text)));
                p_Draw.Invalidate();
            }
            else
            {
                MessageBox.Show("Bitte alle Felder ausfüllen!");
            }
        }

        private void b_Circle_Draw_Click(object sender, EventArgs e)
        {
            Graphics g = p_Draw.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if ((lB_Color.Text != "") && (tB_Thickness.Text != ""))
            {
                /*c = new MyGDICircle(
                                      int.Parse(tB_Circle_x.Text),
                                      int.Parse(tB_Circle_y.Text),
                                      int.Parse(tB_Circle_r.Text),
                                      int.Parse(tB_Circle_r.Text),
                                      lB_Color.Text,
                                      int.Parse(tB_Thickness.Text)
                                      );
                */
                shapes.Add(new MyGDICircle(int.Parse(tB_Circle_x.Text), 
                                              int.Parse(tB_Circle_y.Text), 
                                              int.Parse(tB_Circle_r.Text), 
                                              int.Parse(tB_Circle_r.Text), 
                                              lB_Color.Text, 
                                              int.Parse(tB_Thickness.Text)));
                p_Draw.Invalidate();
            }
            else
            {
                MessageBox.Show("Bitte alle Felder ausfüllen!");
            }
        }

        private void b_Ellipse_Draw_Click(object sender, EventArgs e)
        {
            Graphics g = p_Draw.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if ((lB_Color.Text != "") && (tB_Thickness.Text != ""))
            {
                shapes.Add(new MyGDIEllipse(
                                      int.Parse(tB_Ellipse_x.Text),
                                      int.Parse(tB_Ellipse_y.Text),
                                      int.Parse(tB_Ellipse_r1.Text),
                                      int.Parse(tB_Ellipse_r2.Text),
                                      lB_Color.Text,
                                      int.Parse(tB_Thickness.Text)
                                      ));
                p_Draw.Invalidate();
            }
            else
            {
                MessageBox.Show("Bitte alle Felder ausfüllen!");
            }
        }

        private void b_Rectangle_Draw_Click(object sender, EventArgs e)
        {
            Graphics g = p_Draw.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if ((lB_Color.Text != "") && (tB_Thickness.Text != ""))
            {
                if (lB_Graphics.SelectedItem.ToString() == "Rectangle")
                {
                    /*r = new MyGDIRectangle(
                                    int.Parse(tB_Rectangle_x1.Text),
                                    int.Parse(tB_Rectangle_y1.Text),
                                    int.Parse(tB_Rectangle_Width.Text),
                                    int.Parse(tB_Rectangle_Height.Text),
                                    lB_Color.Text,
                                    int.Parse(tB_Thickness.Text)
                                    );*/
                    shapes.Add(new MyGDIRectangle(int.Parse(tB_Rectangle_x1.Text),
                                                  int.Parse(tB_Rectangle_y1.Text),
                                                  int.Parse(tB_Rectangle_Width.Text),
                                                  int.Parse(tB_Rectangle_Height.Text), 
                                                  lB_Color.Text, 
                                                  int.Parse(tB_Thickness.Text)));
                    p_Draw.Invalidate();
                }
                else
                {
                    MessageBox.Show("Bitte alle Felder ausfüllen!");
                }
            }
        }


        private void lB_Graphics_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lB_Graphics.SelectedItem.ToString() == "Line")
            {
                gB_Line.Enabled = true;
                gB_Circle.Enabled = false;
                gB_Ellipse.Enabled = false;
                gB_Rectangle.Enabled = false;  
            }

            if (lB_Graphics.SelectedItem.ToString() == "Circle")
            {
                gB_Line.Enabled = false;
                gB_Circle.Enabled = true;
                gB_Ellipse.Enabled = false;
                gB_Rectangle.Enabled = false;
            }

            if (lB_Graphics.SelectedItem.ToString() == "Ellipse")
            {
                gB_Line.Enabled = false;
                gB_Circle.Enabled = false;
                gB_Ellipse.Enabled = true;
                gB_Rectangle.Enabled = false;  
            }

            if (lB_Graphics.SelectedItem.ToString() == "Rectangle")
            {
                gB_Line.Enabled = false;
                gB_Circle.Enabled = false;
                gB_Ellipse.Enabled = false;
                gB_Rectangle.Enabled = true;
            }
        }

        private void p_Draw_MouseDown(object sender, MouseEventArgs e)
        {
            foreach(var item in shapes)
            {
                if (item.selected(e.X,e.Y))
                {
                    selectedItem = item;
                    p_Draw.Refresh();
                    item.highlight(p_Draw.CreateGraphics(), this);
                }
                else if (item == shapes.Last())
                {
                    p_Draw.Invalidate();
                }
            }
        }

        private void p_Draw_Paint(object sender, PaintEventArgs e)
        {
            Matrix m = new Matrix();
            m.Scale(Convert.ToSingle(zoombar.Value) / 100, Convert.ToSingle(zoombar.Value) / 100);
            e.Graphics.Transform = m;
            if (shapes.Count() > 0)
            {
                foreach (var item in shapes)
                {
                    item.Draw(e.Graphics);
                }
            }
            if (shapes.Count() > 0)
            {
                foreach (var item in shapes)
                {
                    item.Draw(e.Graphics);
                }
            }
        }

        private void zoombar_Scroll(object sender, EventArgs e)
        {
            p_Draw.Invalidate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (selectedItem != null)
            {
                shapes.Remove(selectedItem);
            p_Draw.Invalidate();
            }
        }
    }
}