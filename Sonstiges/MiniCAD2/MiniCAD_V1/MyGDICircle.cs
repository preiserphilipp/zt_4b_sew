﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCAD_V1
{
    class MyGDICircle : MyCircle, IDrawable
    {
        public MyGDICircle(int x, int y, int r1, int r2, string color, int thickness)
            : base (x, y, r1,r2, color, thickness)
        {

        }
        public void Draw(Graphics g)
        {
            g.DrawEllipse(new Pen(Color.FromName(color), thickness), x, y, r1, r2);
        }
        public bool selected(int mouseX, int mouseY)
        {
            int a = mouseX-x;
            int b = y - mouseY;
            int c = Convert.ToInt32(Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2)));
            if (c <= r1)
                return true;
            return false;
        }
        public void highlight(Graphics g, Form1 f)
        {
            g.DrawRectangle(new Pen(Color.Blue, 2), x - r1 - 5, y - 5, 10, 10);
            g.DrawRectangle(new Pen(Color.Blue, 2), x + r1 - 5, y - 5, 10, 10);
            g.DrawRectangle(new Pen(Color.Blue, 2), x - 5, y - r1 - 5, 10, 10);
            g.DrawRectangle(new Pen(Color.Blue, 2), x - 5, y + r1 - 5, 10, 10);

            f.tB_Circle_x.Text = x.ToString();
            f.tB_Circle_y.Text = y.ToString();
            f.tB_Circle_r.Text = r1.ToString();

            f.tB_Thickness.Text = thickness.ToString();
            f.lB_Color.Text = color.ToString();
        }

        public override string ToString()
        {
            return "Circle";
        }
    }
}
