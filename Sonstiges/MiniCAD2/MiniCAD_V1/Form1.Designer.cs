﻿namespace MiniCAD_V1
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_Rectangle_Draw = new System.Windows.Forms.Button();
            this.l_Line_y1 = new System.Windows.Forms.Label();
            this.l_Circle_x = new System.Windows.Forms.Label();
            this.tB_Line_x1 = new System.Windows.Forms.TextBox();
            this.l_Line_x1 = new System.Windows.Forms.Label();
            this.tB_Line_y1 = new System.Windows.Forms.TextBox();
            this.l_Line_x2 = new System.Windows.Forms.Label();
            this.tB_Line_x2 = new System.Windows.Forms.TextBox();
            this.tB_Line_y2 = new System.Windows.Forms.TextBox();
            this.l_Line_y2 = new System.Windows.Forms.Label();
            this.tB_Circle_x = new System.Windows.Forms.TextBox();
            this.tB_Circle_y = new System.Windows.Forms.TextBox();
            this.tB_Circle_r = new System.Windows.Forms.TextBox();
            this.l_Circle_y = new System.Windows.Forms.Label();
            this.l_Circle_r = new System.Windows.Forms.Label();
            this.tB_Rectangle_x1 = new System.Windows.Forms.TextBox();
            this.tB_Rectangle_y1 = new System.Windows.Forms.TextBox();
            this.l_Rectangle_y1 = new System.Windows.Forms.Label();
            this.l_Rectangle_x1 = new System.Windows.Forms.Label();
            this.l_Color = new System.Windows.Forms.Label();
            this.l_Tickness = new System.Windows.Forms.Label();
            this.tB_Thickness = new System.Windows.Forms.TextBox();
            this.p_Draw = new System.Windows.Forms.Panel();
            this.gB_Rectangle = new System.Windows.Forms.GroupBox();
            this.l_Rectangle = new System.Windows.Forms.Label();
            this.tB_Rectangle_Height = new System.Windows.Forms.TextBox();
            this.tB_Rectangle_Width = new System.Windows.Forms.TextBox();
            this.l_Rectangle_Height = new System.Windows.Forms.Label();
            this.l_Rectangle_Width = new System.Windows.Forms.Label();
            this.gB_Circle = new System.Windows.Forms.GroupBox();
            this.l_Circle = new System.Windows.Forms.Label();
            this.b_Circle_Draw = new System.Windows.Forms.Button();
            this.gB_Line = new System.Windows.Forms.GroupBox();
            this.l_Line = new System.Windows.Forms.Label();
            this.b_Line_Draw = new System.Windows.Forms.Button();
            this.lB_Graphics = new System.Windows.Forms.ListBox();
            this.lB_Color = new System.Windows.Forms.ListBox();
            this.gB_Ellipse = new System.Windows.Forms.GroupBox();
            this.l_Ellipse = new System.Windows.Forms.Label();
            this.tB_Ellipse_r2 = new System.Windows.Forms.TextBox();
            this.l_Ellipse_r2 = new System.Windows.Forms.Label();
            this.b_Ellipse_Draw = new System.Windows.Forms.Button();
            this.tB_Ellipse_y = new System.Windows.Forms.TextBox();
            this.l_Ellipse_x = new System.Windows.Forms.Label();
            this.tB_Ellipse_x = new System.Windows.Forms.TextBox();
            this.tB_Ellipse_r1 = new System.Windows.Forms.TextBox();
            this.l_Ellipse_y = new System.Windows.Forms.Label();
            this.l_Ellipse_r1 = new System.Windows.Forms.Label();
            this.zoombar = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            this.gB_Rectangle.SuspendLayout();
            this.gB_Circle.SuspendLayout();
            this.gB_Line.SuspendLayout();
            this.gB_Ellipse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoombar)).BeginInit();
            this.SuspendLayout();
            // 
            // b_Rectangle_Draw
            // 
            this.b_Rectangle_Draw.Location = new System.Drawing.Point(17, 112);
            this.b_Rectangle_Draw.Name = "b_Rectangle_Draw";
            this.b_Rectangle_Draw.Size = new System.Drawing.Size(135, 23);
            this.b_Rectangle_Draw.TabIndex = 0;
            this.b_Rectangle_Draw.Text = "Draw";
            this.b_Rectangle_Draw.UseVisualStyleBackColor = true;
            this.b_Rectangle_Draw.Click += new System.EventHandler(this.b_Rectangle_Draw_Click);
            // 
            // l_Line_y1
            // 
            this.l_Line_y1.AutoSize = true;
            this.l_Line_y1.Location = new System.Drawing.Point(97, 56);
            this.l_Line_y1.Name = "l_Line_y1";
            this.l_Line_y1.Size = new System.Drawing.Size(21, 13);
            this.l_Line_y1.TabIndex = 4;
            this.l_Line_y1.Text = "y1:";
            // 
            // l_Circle_x
            // 
            this.l_Circle_x.AutoSize = true;
            this.l_Circle_x.Location = new System.Drawing.Point(14, 55);
            this.l_Circle_x.Name = "l_Circle_x";
            this.l_Circle_x.Size = new System.Drawing.Size(15, 13);
            this.l_Circle_x.TabIndex = 5;
            this.l_Circle_x.Text = "x:";
            // 
            // tB_Line_x1
            // 
            this.tB_Line_x1.Location = new System.Drawing.Point(45, 53);
            this.tB_Line_x1.Name = "tB_Line_x1";
            this.tB_Line_x1.Size = new System.Drawing.Size(34, 20);
            this.tB_Line_x1.TabIndex = 6;
            // 
            // l_Line_x1
            // 
            this.l_Line_x1.AutoSize = true;
            this.l_Line_x1.Location = new System.Drawing.Point(18, 56);
            this.l_Line_x1.Name = "l_Line_x1";
            this.l_Line_x1.Size = new System.Drawing.Size(21, 13);
            this.l_Line_x1.TabIndex = 7;
            this.l_Line_x1.Text = "x1:";
            // 
            // tB_Line_y1
            // 
            this.tB_Line_y1.Location = new System.Drawing.Point(124, 53);
            this.tB_Line_y1.Name = "tB_Line_y1";
            this.tB_Line_y1.Size = new System.Drawing.Size(34, 20);
            this.tB_Line_y1.TabIndex = 8;
            // 
            // l_Line_x2
            // 
            this.l_Line_x2.AutoSize = true;
            this.l_Line_x2.Location = new System.Drawing.Point(18, 82);
            this.l_Line_x2.Name = "l_Line_x2";
            this.l_Line_x2.Size = new System.Drawing.Size(21, 13);
            this.l_Line_x2.TabIndex = 9;
            this.l_Line_x2.Text = "x2:";
            // 
            // tB_Line_x2
            // 
            this.tB_Line_x2.Location = new System.Drawing.Point(45, 79);
            this.tB_Line_x2.Name = "tB_Line_x2";
            this.tB_Line_x2.Size = new System.Drawing.Size(34, 20);
            this.tB_Line_x2.TabIndex = 10;
            // 
            // tB_Line_y2
            // 
            this.tB_Line_y2.Location = new System.Drawing.Point(124, 79);
            this.tB_Line_y2.Name = "tB_Line_y2";
            this.tB_Line_y2.Size = new System.Drawing.Size(34, 20);
            this.tB_Line_y2.TabIndex = 11;
            // 
            // l_Line_y2
            // 
            this.l_Line_y2.AutoSize = true;
            this.l_Line_y2.Location = new System.Drawing.Point(97, 82);
            this.l_Line_y2.Name = "l_Line_y2";
            this.l_Line_y2.Size = new System.Drawing.Size(21, 13);
            this.l_Line_y2.TabIndex = 12;
            this.l_Line_y2.Text = "y2:";
            // 
            // tB_Circle_x
            // 
            this.tB_Circle_x.Location = new System.Drawing.Point(41, 52);
            this.tB_Circle_x.Name = "tB_Circle_x";
            this.tB_Circle_x.Size = new System.Drawing.Size(34, 20);
            this.tB_Circle_x.TabIndex = 14;
            // 
            // tB_Circle_y
            // 
            this.tB_Circle_y.Location = new System.Drawing.Point(120, 52);
            this.tB_Circle_y.Name = "tB_Circle_y";
            this.tB_Circle_y.Size = new System.Drawing.Size(34, 20);
            this.tB_Circle_y.TabIndex = 15;
            // 
            // tB_Circle_r
            // 
            this.tB_Circle_r.Location = new System.Drawing.Point(41, 78);
            this.tB_Circle_r.Name = "tB_Circle_r";
            this.tB_Circle_r.Size = new System.Drawing.Size(34, 20);
            this.tB_Circle_r.TabIndex = 16;
            // 
            // l_Circle_y
            // 
            this.l_Circle_y.AutoSize = true;
            this.l_Circle_y.Location = new System.Drawing.Point(93, 55);
            this.l_Circle_y.Name = "l_Circle_y";
            this.l_Circle_y.Size = new System.Drawing.Size(15, 13);
            this.l_Circle_y.TabIndex = 17;
            this.l_Circle_y.Text = "y:";
            // 
            // l_Circle_r
            // 
            this.l_Circle_r.AutoSize = true;
            this.l_Circle_r.Location = new System.Drawing.Point(14, 81);
            this.l_Circle_r.Name = "l_Circle_r";
            this.l_Circle_r.Size = new System.Drawing.Size(13, 13);
            this.l_Circle_r.TabIndex = 18;
            this.l_Circle_r.Text = "r:";
            // 
            // tB_Rectangle_x1
            // 
            this.tB_Rectangle_x1.Location = new System.Drawing.Point(41, 35);
            this.tB_Rectangle_x1.Name = "tB_Rectangle_x1";
            this.tB_Rectangle_x1.Size = new System.Drawing.Size(34, 20);
            this.tB_Rectangle_x1.TabIndex = 21;
            // 
            // tB_Rectangle_y1
            // 
            this.tB_Rectangle_y1.Location = new System.Drawing.Point(120, 35);
            this.tB_Rectangle_y1.Name = "tB_Rectangle_y1";
            this.tB_Rectangle_y1.Size = new System.Drawing.Size(34, 20);
            this.tB_Rectangle_y1.TabIndex = 22;
            // 
            // l_Rectangle_y1
            // 
            this.l_Rectangle_y1.AutoSize = true;
            this.l_Rectangle_y1.Location = new System.Drawing.Point(93, 38);
            this.l_Rectangle_y1.Name = "l_Rectangle_y1";
            this.l_Rectangle_y1.Size = new System.Drawing.Size(21, 13);
            this.l_Rectangle_y1.TabIndex = 27;
            this.l_Rectangle_y1.Text = "y1:";
            // 
            // l_Rectangle_x1
            // 
            this.l_Rectangle_x1.AutoSize = true;
            this.l_Rectangle_x1.Location = new System.Drawing.Point(14, 38);
            this.l_Rectangle_x1.Name = "l_Rectangle_x1";
            this.l_Rectangle_x1.Size = new System.Drawing.Size(21, 13);
            this.l_Rectangle_x1.TabIndex = 29;
            this.l_Rectangle_x1.Text = "x1:";
            // 
            // l_Color
            // 
            this.l_Color.AutoSize = true;
            this.l_Color.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_Color.Location = new System.Drawing.Point(14, 28);
            this.l_Color.Name = "l_Color";
            this.l_Color.Size = new System.Drawing.Size(49, 16);
            this.l_Color.TabIndex = 34;
            this.l_Color.Text = "Color:";
            // 
            // l_Tickness
            // 
            this.l_Tickness.AutoSize = true;
            this.l_Tickness.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_Tickness.Location = new System.Drawing.Point(4, 102);
            this.l_Tickness.Name = "l_Tickness";
            this.l_Tickness.Size = new System.Drawing.Size(83, 16);
            this.l_Tickness.TabIndex = 35;
            this.l_Tickness.Text = "Thickness:";
            // 
            // tB_Thickness
            // 
            this.tB_Thickness.Location = new System.Drawing.Point(93, 101);
            this.tB_Thickness.Name = "tB_Thickness";
            this.tB_Thickness.Size = new System.Drawing.Size(77, 20);
            this.tB_Thickness.TabIndex = 37;
            // 
            // p_Draw
            // 
            this.p_Draw.Location = new System.Drawing.Point(399, 12);
            this.p_Draw.Name = "p_Draw";
            this.p_Draw.Size = new System.Drawing.Size(498, 458);
            this.p_Draw.TabIndex = 38;
            this.p_Draw.Paint += new System.Windows.Forms.PaintEventHandler(this.p_Draw_Paint);
            this.p_Draw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.p_Draw_MouseDown);
            // 
            // gB_Rectangle
            // 
            this.gB_Rectangle.Controls.Add(this.l_Rectangle);
            this.gB_Rectangle.Controls.Add(this.tB_Rectangle_Height);
            this.gB_Rectangle.Controls.Add(this.tB_Rectangle_x1);
            this.gB_Rectangle.Controls.Add(this.tB_Rectangle_y1);
            this.gB_Rectangle.Controls.Add(this.tB_Rectangle_Width);
            this.gB_Rectangle.Controls.Add(this.l_Rectangle_y1);
            this.gB_Rectangle.Controls.Add(this.l_Rectangle_Height);
            this.gB_Rectangle.Controls.Add(this.l_Rectangle_x1);
            this.gB_Rectangle.Controls.Add(this.b_Rectangle_Draw);
            this.gB_Rectangle.Controls.Add(this.l_Rectangle_Width);
            this.gB_Rectangle.Location = new System.Drawing.Point(17, 301);
            this.gB_Rectangle.Name = "gB_Rectangle";
            this.gB_Rectangle.Size = new System.Drawing.Size(165, 156);
            this.gB_Rectangle.TabIndex = 2;
            this.gB_Rectangle.TabStop = false;
            // 
            // l_Rectangle
            // 
            this.l_Rectangle.AutoSize = true;
            this.l_Rectangle.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_Rectangle.Location = new System.Drawing.Point(-5, 0);
            this.l_Rectangle.Name = "l_Rectangle";
            this.l_Rectangle.Size = new System.Drawing.Size(111, 29);
            this.l_Rectangle.TabIndex = 0;
            this.l_Rectangle.Text = "Rectangle";
            // 
            // tB_Rectangle_Height
            // 
            this.tB_Rectangle_Height.Location = new System.Drawing.Point(58, 87);
            this.tB_Rectangle_Height.Name = "tB_Rectangle_Height";
            this.tB_Rectangle_Height.Size = new System.Drawing.Size(96, 20);
            this.tB_Rectangle_Height.TabIndex = 26;
            // 
            // tB_Rectangle_Width
            // 
            this.tB_Rectangle_Width.Location = new System.Drawing.Point(58, 61);
            this.tB_Rectangle_Width.Name = "tB_Rectangle_Width";
            this.tB_Rectangle_Width.Size = new System.Drawing.Size(96, 20);
            this.tB_Rectangle_Width.TabIndex = 25;
            // 
            // l_Rectangle_Height
            // 
            this.l_Rectangle_Height.AutoSize = true;
            this.l_Rectangle_Height.Location = new System.Drawing.Point(14, 87);
            this.l_Rectangle_Height.Name = "l_Rectangle_Height";
            this.l_Rectangle_Height.Size = new System.Drawing.Size(41, 13);
            this.l_Rectangle_Height.TabIndex = 32;
            this.l_Rectangle_Height.Text = "Height:";
            // 
            // l_Rectangle_Width
            // 
            this.l_Rectangle_Width.AutoSize = true;
            this.l_Rectangle_Width.Location = new System.Drawing.Point(14, 64);
            this.l_Rectangle_Width.Name = "l_Rectangle_Width";
            this.l_Rectangle_Width.Size = new System.Drawing.Size(38, 13);
            this.l_Rectangle_Width.TabIndex = 31;
            this.l_Rectangle_Width.Text = "Width:";
            // 
            // gB_Circle
            // 
            this.gB_Circle.Controls.Add(this.l_Circle);
            this.gB_Circle.Controls.Add(this.b_Circle_Draw);
            this.gB_Circle.Controls.Add(this.tB_Circle_y);
            this.gB_Circle.Controls.Add(this.l_Circle_x);
            this.gB_Circle.Controls.Add(this.tB_Circle_x);
            this.gB_Circle.Controls.Add(this.tB_Circle_r);
            this.gB_Circle.Controls.Add(this.l_Circle_y);
            this.gB_Circle.Controls.Add(this.l_Circle_r);
            this.gB_Circle.Location = new System.Drawing.Point(200, 139);
            this.gB_Circle.Name = "gB_Circle";
            this.gB_Circle.Size = new System.Drawing.Size(165, 156);
            this.gB_Circle.TabIndex = 1;
            this.gB_Circle.TabStop = false;
            // 
            // l_Circle
            // 
            this.l_Circle.AutoSize = true;
            this.l_Circle.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_Circle.Location = new System.Drawing.Point(-3, 0);
            this.l_Circle.Name = "l_Circle";
            this.l_Circle.Size = new System.Drawing.Size(71, 29);
            this.l_Circle.TabIndex = 35;
            this.l_Circle.Text = "Circle";
            // 
            // b_Circle_Draw
            // 
            this.b_Circle_Draw.Location = new System.Drawing.Point(17, 109);
            this.b_Circle_Draw.Name = "b_Circle_Draw";
            this.b_Circle_Draw.Size = new System.Drawing.Size(137, 23);
            this.b_Circle_Draw.TabIndex = 33;
            this.b_Circle_Draw.Text = "Draw";
            this.b_Circle_Draw.UseVisualStyleBackColor = true;
            this.b_Circle_Draw.Click += new System.EventHandler(this.b_Circle_Draw_Click);
            // 
            // gB_Line
            // 
            this.gB_Line.Controls.Add(this.l_Line);
            this.gB_Line.Controls.Add(this.b_Line_Draw);
            this.gB_Line.Controls.Add(this.tB_Line_y2);
            this.gB_Line.Controls.Add(this.l_Line_y1);
            this.gB_Line.Controls.Add(this.tB_Line_x1);
            this.gB_Line.Controls.Add(this.l_Line_x1);
            this.gB_Line.Controls.Add(this.tB_Line_y1);
            this.gB_Line.Controls.Add(this.l_Line_x2);
            this.gB_Line.Controls.Add(this.tB_Line_x2);
            this.gB_Line.Controls.Add(this.l_Line_y2);
            this.gB_Line.Location = new System.Drawing.Point(13, 139);
            this.gB_Line.Name = "gB_Line";
            this.gB_Line.Size = new System.Drawing.Size(165, 156);
            this.gB_Line.TabIndex = 0;
            this.gB_Line.TabStop = false;
            // 
            // l_Line
            // 
            this.l_Line.AutoSize = true;
            this.l_Line.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_Line.Location = new System.Drawing.Point(-4, 0);
            this.l_Line.Name = "l_Line";
            this.l_Line.Size = new System.Drawing.Size(54, 29);
            this.l_Line.TabIndex = 36;
            this.l_Line.Text = "Line";
            // 
            // b_Line_Draw
            // 
            this.b_Line_Draw.Location = new System.Drawing.Point(21, 109);
            this.b_Line_Draw.Name = "b_Line_Draw";
            this.b_Line_Draw.Size = new System.Drawing.Size(137, 23);
            this.b_Line_Draw.TabIndex = 34;
            this.b_Line_Draw.Text = "Draw";
            this.b_Line_Draw.UseVisualStyleBackColor = true;
            this.b_Line_Draw.Click += new System.EventHandler(this.b_Line_Draw_Click);
            // 
            // lB_Graphics
            // 
            this.lB_Graphics.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lB_Graphics.FormattingEnabled = true;
            this.lB_Graphics.ItemHeight = 16;
            this.lB_Graphics.Items.AddRange(new object[] {
            "Line",
            "Circle",
            "Ellipse",
            "Rectangle"});
            this.lB_Graphics.Location = new System.Drawing.Point(188, 12);
            this.lB_Graphics.Name = "lB_Graphics";
            this.lB_Graphics.Size = new System.Drawing.Size(160, 68);
            this.lB_Graphics.TabIndex = 39;
            this.lB_Graphics.SelectedIndexChanged += new System.EventHandler(this.lB_Graphics_SelectedIndexChanged);
            // 
            // lB_Color
            // 
            this.lB_Color.FormattingEnabled = true;
            this.lB_Color.Items.AddRange(new object[] {
            "black",
            "blue",
            "brown",
            "green",
            "grey",
            "orange",
            "red",
            "violet",
            "white",
            "yellow"});
            this.lB_Color.Location = new System.Drawing.Point(69, 12);
            this.lB_Color.Name = "lB_Color";
            this.lB_Color.Size = new System.Drawing.Size(109, 69);
            this.lB_Color.TabIndex = 40;
            // 
            // gB_Ellipse
            // 
            this.gB_Ellipse.Controls.Add(this.l_Ellipse);
            this.gB_Ellipse.Controls.Add(this.tB_Ellipse_r2);
            this.gB_Ellipse.Controls.Add(this.l_Ellipse_r2);
            this.gB_Ellipse.Controls.Add(this.b_Ellipse_Draw);
            this.gB_Ellipse.Controls.Add(this.tB_Ellipse_y);
            this.gB_Ellipse.Controls.Add(this.l_Ellipse_x);
            this.gB_Ellipse.Controls.Add(this.tB_Ellipse_x);
            this.gB_Ellipse.Controls.Add(this.tB_Ellipse_r1);
            this.gB_Ellipse.Controls.Add(this.l_Ellipse_y);
            this.gB_Ellipse.Controls.Add(this.l_Ellipse_r1);
            this.gB_Ellipse.Location = new System.Drawing.Point(200, 301);
            this.gB_Ellipse.Name = "gB_Ellipse";
            this.gB_Ellipse.Size = new System.Drawing.Size(165, 156);
            this.gB_Ellipse.TabIndex = 34;
            this.gB_Ellipse.TabStop = false;
            // 
            // l_Ellipse
            // 
            this.l_Ellipse.AutoSize = true;
            this.l_Ellipse.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_Ellipse.Location = new System.Drawing.Point(-3, 0);
            this.l_Ellipse.Name = "l_Ellipse";
            this.l_Ellipse.Size = new System.Drawing.Size(77, 29);
            this.l_Ellipse.TabIndex = 33;
            this.l_Ellipse.Text = "Ellipse";
            // 
            // tB_Ellipse_r2
            // 
            this.tB_Ellipse_r2.Location = new System.Drawing.Point(120, 61);
            this.tB_Ellipse_r2.Name = "tB_Ellipse_r2";
            this.tB_Ellipse_r2.Size = new System.Drawing.Size(34, 20);
            this.tB_Ellipse_r2.TabIndex = 35;
            // 
            // l_Ellipse_r2
            // 
            this.l_Ellipse_r2.AutoSize = true;
            this.l_Ellipse_r2.Location = new System.Drawing.Point(98, 64);
            this.l_Ellipse_r2.Name = "l_Ellipse_r2";
            this.l_Ellipse_r2.Size = new System.Drawing.Size(19, 13);
            this.l_Ellipse_r2.TabIndex = 34;
            this.l_Ellipse_r2.Text = "r2:";
            // 
            // b_Ellipse_Draw
            // 
            this.b_Ellipse_Draw.Location = new System.Drawing.Point(22, 112);
            this.b_Ellipse_Draw.Name = "b_Ellipse_Draw";
            this.b_Ellipse_Draw.Size = new System.Drawing.Size(137, 23);
            this.b_Ellipse_Draw.TabIndex = 33;
            this.b_Ellipse_Draw.Text = "Draw";
            this.b_Ellipse_Draw.UseVisualStyleBackColor = true;
            this.b_Ellipse_Draw.Click += new System.EventHandler(this.b_Ellipse_Draw_Click);
            // 
            // tB_Ellipse_y
            // 
            this.tB_Ellipse_y.Location = new System.Drawing.Point(120, 35);
            this.tB_Ellipse_y.Name = "tB_Ellipse_y";
            this.tB_Ellipse_y.Size = new System.Drawing.Size(34, 20);
            this.tB_Ellipse_y.TabIndex = 15;
            // 
            // l_Ellipse_x
            // 
            this.l_Ellipse_x.AutoSize = true;
            this.l_Ellipse_x.Location = new System.Drawing.Point(19, 38);
            this.l_Ellipse_x.Name = "l_Ellipse_x";
            this.l_Ellipse_x.Size = new System.Drawing.Size(15, 13);
            this.l_Ellipse_x.TabIndex = 5;
            this.l_Ellipse_x.Text = "x:";
            // 
            // tB_Ellipse_x
            // 
            this.tB_Ellipse_x.Location = new System.Drawing.Point(46, 35);
            this.tB_Ellipse_x.Name = "tB_Ellipse_x";
            this.tB_Ellipse_x.Size = new System.Drawing.Size(34, 20);
            this.tB_Ellipse_x.TabIndex = 14;
            // 
            // tB_Ellipse_r1
            // 
            this.tB_Ellipse_r1.Location = new System.Drawing.Point(46, 61);
            this.tB_Ellipse_r1.Name = "tB_Ellipse_r1";
            this.tB_Ellipse_r1.Size = new System.Drawing.Size(34, 20);
            this.tB_Ellipse_r1.TabIndex = 16;
            // 
            // l_Ellipse_y
            // 
            this.l_Ellipse_y.AutoSize = true;
            this.l_Ellipse_y.Location = new System.Drawing.Point(98, 38);
            this.l_Ellipse_y.Name = "l_Ellipse_y";
            this.l_Ellipse_y.Size = new System.Drawing.Size(15, 13);
            this.l_Ellipse_y.TabIndex = 17;
            this.l_Ellipse_y.Text = "y:";
            // 
            // l_Ellipse_r1
            // 
            this.l_Ellipse_r1.AutoSize = true;
            this.l_Ellipse_r1.Location = new System.Drawing.Point(19, 64);
            this.l_Ellipse_r1.Name = "l_Ellipse_r1";
            this.l_Ellipse_r1.Size = new System.Drawing.Size(19, 13);
            this.l_Ellipse_r1.TabIndex = 18;
            this.l_Ellipse_r1.Text = "r1:";
            // 
            // zoombar
            // 
            this.zoombar.Location = new System.Drawing.Point(188, 91);
            this.zoombar.Maximum = 1000;
            this.zoombar.Minimum = 100;
            this.zoombar.Name = "zoombar";
            this.zoombar.Size = new System.Drawing.Size(104, 45);
            this.zoombar.TabIndex = 41;
            this.zoombar.Value = 100;
            this.zoombar.Scroll += new System.EventHandler(this.zoombar_Scroll);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(296, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 42;
            this.button1.Text = "Delete Selected";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 482);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.zoombar);
            this.Controls.Add(this.gB_Ellipse);
            this.Controls.Add(this.lB_Color);
            this.Controls.Add(this.gB_Circle);
            this.Controls.Add(this.gB_Rectangle);
            this.Controls.Add(this.lB_Graphics);
            this.Controls.Add(this.p_Draw);
            this.Controls.Add(this.gB_Line);
            this.Controls.Add(this.tB_Thickness);
            this.Controls.Add(this.l_Tickness);
            this.Controls.Add(this.l_Color);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gB_Rectangle.ResumeLayout(false);
            this.gB_Rectangle.PerformLayout();
            this.gB_Circle.ResumeLayout(false);
            this.gB_Circle.PerformLayout();
            this.gB_Line.ResumeLayout(false);
            this.gB_Line.PerformLayout();
            this.gB_Ellipse.ResumeLayout(false);
            this.gB_Ellipse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoombar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_Rectangle_Draw;
        private System.Windows.Forms.Label l_Line_y1;
        private System.Windows.Forms.Label l_Circle_x;
        private System.Windows.Forms.Label l_Line_x1;
        private System.Windows.Forms.Label l_Line_x2;
        private System.Windows.Forms.Label l_Line_y2;
        private System.Windows.Forms.Label l_Circle_y;
        private System.Windows.Forms.Label l_Circle_r;
        private System.Windows.Forms.Label l_Rectangle_y1;
        private System.Windows.Forms.Label l_Rectangle_x1;
        private System.Windows.Forms.Label l_Color;
        private System.Windows.Forms.Label l_Tickness;
        private System.Windows.Forms.Panel p_Draw;
        private System.Windows.Forms.Label l_Rectangle_Width;
        private System.Windows.Forms.Label l_Rectangle_Height;
        private System.Windows.Forms.ListBox lB_Graphics;
        private System.Windows.Forms.Button b_Circle_Draw;
        private System.Windows.Forms.Button b_Line_Draw;
        private System.Windows.Forms.Label l_Rectangle;
        private System.Windows.Forms.Label l_Circle;
        private System.Windows.Forms.Label l_Line;
        private System.Windows.Forms.Label l_Ellipse;
        private System.Windows.Forms.Label l_Ellipse_r2;
        private System.Windows.Forms.Button b_Ellipse_Draw;
        private System.Windows.Forms.Label l_Ellipse_x;
        private System.Windows.Forms.Label l_Ellipse_y;
        private System.Windows.Forms.Label l_Ellipse_r1;
        private System.Windows.Forms.TrackBar zoombar;
        public System.Windows.Forms.TextBox tB_Line_x1;
        public System.Windows.Forms.TextBox tB_Line_y1;
        public System.Windows.Forms.TextBox tB_Line_x2;
        public System.Windows.Forms.TextBox tB_Line_y2;
        public System.Windows.Forms.TextBox tB_Circle_x;
        public System.Windows.Forms.TextBox tB_Circle_y;
        public System.Windows.Forms.TextBox tB_Circle_r;
        public System.Windows.Forms.TextBox tB_Rectangle_x1;
        public System.Windows.Forms.TextBox tB_Rectangle_y1;
        public System.Windows.Forms.TextBox tB_Thickness;
        public System.Windows.Forms.TextBox tB_Rectangle_Width;
        public System.Windows.Forms.TextBox tB_Rectangle_Height;
        public System.Windows.Forms.GroupBox gB_Circle;
        public System.Windows.Forms.GroupBox gB_Line;
        public System.Windows.Forms.GroupBox gB_Rectangle;
        public System.Windows.Forms.ListBox lB_Color;
        public System.Windows.Forms.GroupBox gB_Ellipse;
        public System.Windows.Forms.TextBox tB_Ellipse_r2;
        public System.Windows.Forms.TextBox tB_Ellipse_y;
        public System.Windows.Forms.TextBox tB_Ellipse_x;
        public System.Windows.Forms.TextBox tB_Ellipse_r1;
        private System.Windows.Forms.Button button1;
    }
}

