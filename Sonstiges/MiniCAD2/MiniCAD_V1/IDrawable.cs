﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MiniCAD_V1
{
    interface IDrawable
    {
        void Draw(Graphics g);
        bool selected(int mouseX, int mouseY);
        void highlight(Graphics g, Form1 f);
    }
}
