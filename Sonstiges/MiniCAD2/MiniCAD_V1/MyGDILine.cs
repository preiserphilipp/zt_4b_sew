﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCAD_V1
{
    class MyGDILine : MyLine, IDrawable
    {
        public MyGDILine(int x, int y, int ex, int ey, string color, int thickness)
            : base (x, y, ex, ey, color, thickness)
        {
        
        }
        public bool selected(int mouseX, int mouseY) 
        {
            double gegenkathL = ey - y;
            double ankathL = ex - x;
            double winkelL = Math.Atan2(gegenkathL, ankathL);

            double gegenkathM = mouseY - y;
            double ankathM = mouseX - x;
            double winkelM = Math.Atan2(gegenkathM, ankathM);

            double klickLaenge = Math.Sqrt(Math.Pow(mouseX - x, 2) + Math.Pow(mouseY - y, 2));
            double linieLaenge = Math.Sqrt(Math.Pow(ex - x, 2) + Math.Pow(ey - y, 2));

            if (winkelM < winkelL && winkelM > winkelL && klickLaenge < linieLaenge)
                return true;
            return false;
        }
        public void highlight(Graphics g, Form1 f)
        {
            g.DrawRectangle(new Pen(Color.Blue, 2), x - 5, y - 5, 10, 10);
            g.DrawRectangle(new Pen(Color.Blue, 2), ex - 5, ey - 5, 10, 10);

            f.tB_Line_x1.Text = x.ToString();
            f.tB_Line_y1.Text = y.ToString();
            f.tB_Line_x2.Text = ex.ToString();
            f.tB_Line_y2.Text = ey.ToString();

            f.tB_Thickness.Text = thickness.ToString();
            f.lB_Color.Text = color.ToString();
        }
        public void Draw(Graphics g)
        {
            g.DrawLine(new Pen(Color.FromName(color), thickness), x, y, ex, ey);
        }

        public override string ToString()
        {
            return "Line";
        }
    }
}
