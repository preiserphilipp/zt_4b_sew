﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObjects
{
    public class MyCircle : GraphObject
    {
        protected int r1,r2;

        public MyCircle(int x, int y, int r1, int r2, string color, int thickness)
        {
            this.x = x ;
            this.y = y;
            this.r1 = r1;
            this.r2 = r1;
            this.color = color;
            this.thickness = thickness;
        }
    }
}
