﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObjects
{
    public class MyLine : GraphObject
    {
        protected int ex, ey;

        public MyLine(int x, int y, int ex, int ey, string color, int thickness)
        {
            this.x = x;
            this.y = y;
            this.ex = ex;
            this.ey = ey;
            this.color = color;
            this.thickness = thickness;
        }
    }
}
