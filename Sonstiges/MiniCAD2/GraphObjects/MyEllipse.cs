﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObjects
{
    public class MyEllipse : MyCircle
    {
        protected int r1, r2;

        public MyEllipse(int x, int y, int r1, int r2, string color, int thickness)
            : base (x, y, r1, r2, color, thickness)
        {
            this.x = x;
            this.y = y;
            this.r1 = r1;
            this.r2 = r2;
            this.color = color;
            this.thickness = thickness;
        }
    }
}
