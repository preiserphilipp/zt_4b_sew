﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObjects
{
    public class MyRectangle : GraphObject
    {
        protected int width, height;

        public MyRectangle(int x, int y, int width, int height, string color, int thickness)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.color = color;
            this.thickness = thickness;
        }
    }
}
