﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObjects
{
    public abstract class GraphObject
    {
        protected int x, y;
        protected string color;     // Rahmenfarbe
        protected int thickness;    // Strichstärke
    }
}
