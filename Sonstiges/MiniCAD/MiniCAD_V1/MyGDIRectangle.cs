﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCAD_V1
{
    class MyGDIRectangle : MyRectangle, IDrawable
    {
        public MyGDIRectangle(int x, int y, int width, int height, string color, int thickness)
            : base (x, y, width, height, color, thickness)
        {

        }

        public void Draw(Graphics g)
        {
            g.DrawRectangle(new Pen(Color.FromName(color), thickness), x, y, width, height);
        }

        public override string ToString()
        {
            return "Rectangle";
        }
    }
}
