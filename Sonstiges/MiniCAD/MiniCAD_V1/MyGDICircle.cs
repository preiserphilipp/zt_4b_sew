﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCAD_V1
{
    class MyGDICircle : MyCircle, IDrawable
    {
        public MyGDICircle(int x, int y, int r1, int r2, string color, int thickness)
            : base (x, y, r1, r2, color, thickness)
        {

        }

        public void Draw(Graphics g)
        {
            g.DrawEllipse(new Pen(Color.FromName(color), thickness), x, y, r1, r2);
        }

        public override string ToString()
        {
            return "Circle";
        }
    }
}
