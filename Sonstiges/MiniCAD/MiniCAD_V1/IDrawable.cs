﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MiniCAD_V1
{
    interface IDrawable
    {
        void Draw(Graphics g);
    }
}
