﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCAD_V1
{
    class MyGDILine : MyLine, IDrawable
    {
        public MyGDILine(int x, int y, int ex, int ey, string color, int thickness)
            : base (x, y, ex, ey, color, thickness)
        {
        
        }

        public void Draw(Graphics g)
        {
            g.DrawLine(new Pen(Color.FromName(color), thickness), x, y, ex, ey);
        }

        public override string ToString()
        {
            return "Line";
        }
    }
}
