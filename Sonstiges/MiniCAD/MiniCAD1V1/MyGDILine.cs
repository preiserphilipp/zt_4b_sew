﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObject;
using System.Drawing;

namespace MiniCAD1V1
{
    class MyGDILine:MyLine,IDrawable
    {
        public MyGDILine(int x, int y, int x2, int y2,string f, int th)
            : base(x, y, x2, y2,f,th)
        { 
        }
        public void Draw(Graphics g)
        {
            g.DrawLine(new Pen(Color.FromName(farbe), thickness), x, y, x2, y2);
        }
    }
}
