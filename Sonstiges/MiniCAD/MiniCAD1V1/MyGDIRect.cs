﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObject;
using System.Drawing;

namespace MiniCAD1V1
{
    class MyGDIRect:MyRect,IDrawable
    {
        public MyGDIRect(int x, int y, int w, int h,string f, int th)
            :base(x, y, w, h, f,th)
        {
        }
        public void Draw(Graphics g)
        {
            g.DrawRectangle(new Pen(Color.FromName(farbe),thickness),x,y,w,h);
        }
    }
}
