﻿namespace MiniCAD1V1
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.rectx1 = new System.Windows.Forms.TextBox();
            this.recty1 = new System.Windows.Forms.TextBox();
            this.rectw = new System.Windows.Forms.TextBox();
            this.recth = new System.Windows.Forms.TextBox();
            this.liney2 = new System.Windows.Forms.TextBox();
            this.linex2 = new System.Windows.Forms.TextBox();
            this.thtb = new System.Windows.Forms.TextBox();
            this.ftb = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.linex1 = new System.Windows.Forms.TextBox();
            this.liney1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // rectx1
            // 
            this.rectx1.Location = new System.Drawing.Point(59, 119);
            this.rectx1.Name = "rectx1";
            this.rectx1.Size = new System.Drawing.Size(35, 20);
            this.rectx1.TabIndex = 2;
            this.rectx1.Text = "0";
            // 
            // recty1
            // 
            this.recty1.Location = new System.Drawing.Point(143, 118);
            this.recty1.Name = "recty1";
            this.recty1.Size = new System.Drawing.Size(38, 20);
            this.recty1.TabIndex = 3;
            this.recty1.Text = "0";
            // 
            // rectw
            // 
            this.rectw.Location = new System.Drawing.Point(58, 145);
            this.rectw.Name = "rectw";
            this.rectw.Size = new System.Drawing.Size(36, 20);
            this.rectw.TabIndex = 4;
            this.rectw.Text = "0";
            // 
            // recth
            // 
            this.recth.Location = new System.Drawing.Point(143, 144);
            this.recth.Name = "recth";
            this.recth.Size = new System.Drawing.Size(38, 20);
            this.recth.TabIndex = 5;
            this.recth.Text = "0";
            // 
            // liney2
            // 
            this.liney2.Location = new System.Drawing.Point(144, 271);
            this.liney2.Name = "liney2";
            this.liney2.Size = new System.Drawing.Size(37, 20);
            this.liney2.TabIndex = 9;
            this.liney2.Text = "0";
            // 
            // linex2
            // 
            this.linex2.Location = new System.Drawing.Point(57, 271);
            this.linex2.Name = "linex2";
            this.linex2.Size = new System.Drawing.Size(37, 20);
            this.linex2.TabIndex = 8;
            this.linex2.Text = "0";
            // 
            // thtb
            // 
            this.thtb.Location = new System.Drawing.Point(58, 53);
            this.thtb.Name = "thtb";
            this.thtb.Size = new System.Drawing.Size(100, 20);
            this.thtb.TabIndex = 1;
            this.thtb.Text = "0";
            // 
            // ftb
            // 
            this.ftb.Location = new System.Drawing.Point(58, 27);
            this.ftb.Name = "ftb";
            this.ftb.Size = new System.Drawing.Size(100, 20);
            this.ftb.TabIndex = 0;
            this.ftb.Text = "White";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(58, 307);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Draw";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "x1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "y1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "w";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(102, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "h";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "f";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "th";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 278);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "x2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(103, 278);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "y2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(104, 252);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(18, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "y1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 252);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "x1";
            // 
            // linex1
            // 
            this.linex1.Location = new System.Drawing.Point(57, 245);
            this.linex1.Name = "linex1";
            this.linex1.Size = new System.Drawing.Size(37, 20);
            this.linex1.TabIndex = 6;
            this.linex1.Text = "0";
            // 
            // liney1
            // 
            this.liney1.Location = new System.Drawing.Point(145, 245);
            this.liney1.Name = "liney1";
            this.liney1.Size = new System.Drawing.Size(36, 20);
            this.liney1.TabIndex = 7;
            this.liney1.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 219);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Line";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 102);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Rectangle";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "Style";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(257, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(407, 303);
            this.panel1.TabIndex = 24;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 352);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.linex1);
            this.Controls.Add(this.liney1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ftb);
            this.Controls.Add(this.thtb);
            this.Controls.Add(this.linex2);
            this.Controls.Add(this.liney2);
            this.Controls.Add(this.recth);
            this.Controls.Add(this.rectw);
            this.Controls.Add(this.recty1);
            this.Controls.Add(this.rectx1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox rectx1;
        private System.Windows.Forms.TextBox recty1;
        private System.Windows.Forms.TextBox rectw;
        private System.Windows.Forms.TextBox recth;
        private System.Windows.Forms.TextBox liney2;
        private System.Windows.Forms.TextBox linex2;
        private System.Windows.Forms.TextBox thtb;
        private System.Windows.Forms.TextBox ftb;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox linex1;
        private System.Windows.Forms.TextBox liney1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel1;
    }
}

