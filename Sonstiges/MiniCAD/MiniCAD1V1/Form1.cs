﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GraphObject;

namespace MiniCAD1V1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        MyGDIRect r;
        MyGDILine l;
        private void button1_Click(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            r = new MyGDIRect(int.Parse(rectx1.Text),
                              int.Parse(recty1.Text),
                              int.Parse(rectw.Text),
                              int.Parse(recth.Text),
                              ftb.Text,
                              int.Parse(thtb.Text));

            l = new MyGDILine(int.Parse(linex1.Text),
                              int.Parse(liney1.Text),
                              int.Parse(linex2.Text),
                              int.Parse(liney2.Text),
                              ftb.Text,
                              int.Parse(thtb.Text));
            r.Draw(e.Graphics);
            l.Draw(e.Graphics);
        }
    }
}
