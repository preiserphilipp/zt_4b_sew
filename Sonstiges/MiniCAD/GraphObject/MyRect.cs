﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObject
{
    public class MyRect:GraphObject
    {
        protected int w, h;

        public MyRect(int x, int y, int w, int h, string f, int th)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.thickness = th;
            this.farbe = f;
        }
    }
}
