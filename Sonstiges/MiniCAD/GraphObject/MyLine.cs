﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObject
{
    public class MyLine:GraphObject
    {
        protected int x2, y2;

        public MyLine(int x, int y, int x2, int y2,string f, int th)
        {
            this.x = x;
            this.y = y;
            this.x2 = x2;
            this.y2 = y2;
            this.farbe = f;
            this.thickness = th;
        }
    }
}
