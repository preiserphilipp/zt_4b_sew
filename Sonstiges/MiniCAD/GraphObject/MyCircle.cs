﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObject
{
    public class MyCircle:GraphObject
    {
        protected int rad;
        public MyCircle(int x, int y, int r, string f, int th)
        {
            this.x = x;
            this.y = y;
            this.rad = r;
            this.farbe = f;
            this.thickness = th;
        }
    }
}
