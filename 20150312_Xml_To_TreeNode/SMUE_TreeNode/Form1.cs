﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace SMUE_TreeNode
{
    public partial class Form1 : Form
    {
        XmlDocument doc = new XmlDocument();
        public Form1()
        {
            InitializeComponent();
            doc.LoadXml("<DIR name='HL'> <DIR name='IT'> <FILE name='4BHIT'></FILE> </DIR> <DIR name='Bau'> <FILE name='1CH'></FILE> <FILE name='1BH'></FILE></DIR></DIR>");
            fillTreeView(doc);
        }

        void fillTreeView(XmlDocument doc) {
            XmlNode root = doc.SelectSingleNode("/DIR");
            TreeNode rootNode = new TreeNode(root.Attributes[0].InnerText);
            treeView1.Nodes.Add(rootNode);
            fillMe(root, rootNode);
        }

        void fillMe(XmlNode node, TreeNode tn)
        {
            // Erstelle den fehlenden rekursiven Programmcode, um den Treeview korrekt zu befüllen
            foreach (XmlNode item in node.ChildNodes)
            {
                if (item.Name == "DIR")
                {
                    TreeNode erstellt = tn.Nodes.Add(item.Attributes[0].InnerText);
                    fillMe(item, erstellt);
                }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                string erg = "";//StringBuilder wäre schöner
                string pfad = treeView1.SelectedNode.FullPath;
                //ersetze \ durch / und hänge / vorne dazu, damit selectsinglenode das versteht
                pfad = "/" + pfad;
                pfad = pfad.Replace('\\','/');

                //zerschneiden - mache aus /htl/bau => 
                string[] schnipsel = pfad.Split('/');
                string neu = "";
                foreach (var item in schnipsel)
                {
                    if(item!="")
                    {
                        neu += "/DIR[@name='"+item+"']";
                    }
                }
                //neu hat das format /DIR[@name='htl']/DIR[@name='bau']
                XmlNode ausgewählt = doc.SelectSingleNode(neu);
                foreach (XmlNode item in ausgewählt)
                {
                    if(item.Name=="FILE")
                    erg += item.Attributes[0].InnerText + " "; //statt msgbox files im ListView anzeigen
                }
                if (erg != "")
                {
                    MessageBox.Show(erg);
                }
            }
            catch (Exception ee) { }
        }



    }
}
