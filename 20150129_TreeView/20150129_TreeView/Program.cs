﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _20150129_TreeView
{
    class Program
    {
        static StringBuilder sb = new StringBuilder();
        static void Main(string[] args)
        {
            string path = @"C:\Users\Philipp\Desktop\asdf";
            //ausgabe(path);
            Console.WriteLine(Maketree(path, 0));
            Console.ReadLine();
        }
        static void ausgabe(string path)
        {
            Console.WriteLine(path);
            DirectoryInfo di = new DirectoryInfo(path);
            foreach (var item in di.GetDirectories())
                ausgabe(item.FullName);
        }
        static string Maketree(string path, int tiefe)
        {
            FileInfo fi1 = new FileInfo(path);
            DirectoryInfo di = new DirectoryInfo(path);
            
            string tab = "";

            for (int i = 1; i <= tiefe; i++)//tab verlängern für die Größe von tiefe
                sb.Append("\t");

            sb.Append(di.Name + "\n");//am ende der tabs den namen des aktuellen PFades hinzufügen
            tiefe++;
            foreach(var item in di.GetDirectories())
            {
                //rufe rekursion auf

                Maketree(item.FullName, tiefe);
                
            }
            return sb.ToString();
        }
    }
}
