﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Net.Sockets;

namespace _20150319_EinfacherClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TcpClient client = new TcpClient("localhost", 4711);
            try
            {
                Stream s = client.GetStream();
                s.ReadTimeout = 100;
                StreamReader sr = new StreamReader(s);
                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;

                sw.WriteLine("GET /" + textBox1.Text+".txt HTTP/1.1");
                MessageBox.Show(sr.ReadLine());
                s.Close();
            }
            finally 
            {
                client.Close();
            }   
        }
    }
}
