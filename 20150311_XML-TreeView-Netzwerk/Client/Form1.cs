﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Net.Sockets;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.Columns.Add("name");
            listView1.Columns.Add("size");  
        }
        XmlDocument doc = new XmlDocument();
        
        void fillTreeView(XmlDocument doc)
        {
            XmlNode root = doc.SelectSingleNode("/DIR");
            TreeNode rootNode = new TreeNode(root.Attributes[0].InnerText);
            treeView.Nodes.Add(rootNode);
            fillMe(root, rootNode);
        }

        void fillMe(XmlNode node, TreeNode tn)
        {
            // Erstelle den fehlenden rekursiven Programmcode, um den Treeview korrekt zu befüllen
            foreach (XmlNode item in node.ChildNodes)
            {
                if (item.Name == "DIR")
                {
                    TreeNode erstellt = tn.Nodes.Add(item.Attributes[0].InnerText);
                    erstellt.Tag = item.Attributes[0].InnerText;
                    fillMe(item, erstellt);
                }
            }
        }
        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                listView1.Items.Clear();
                TreeNode tnode = (TreeNode)e.Node;
                XmlNode xnode = (XmlNode)tnode.Tag;
                foreach (XmlNode item in xnode.SelectNodes("file"))
                {
                    string[] listInput = { item.Attributes["name"].InnerText, item.Attributes["size"].InnerText };
                    ListViewItem listItem = new ListViewItem(listInput);
                    listView1.Items.Add(listItem);
                }
            }
            catch { }
        }
        private void b_treeview_Click(object sender, EventArgs e)
        {
            TcpClient client = new TcpClient("localhost", 2055);
            try
            {
                Stream s = client.GetStream();
                s.ReadTimeout = 100;
                StreamReader sr = new StreamReader(s);

                doc.LoadXml(sr.ReadLine());
                fillTreeView(doc);

                s.Close();
            }
            finally
            {
                // code in finally block is guranteed 
                // to execute irrespective of 
                // whether any exception occurs or does 
                // not occur in the try block
                client.Close();
            }
        }
    }
}
