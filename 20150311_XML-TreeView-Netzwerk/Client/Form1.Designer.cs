﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView = new System.Windows.Forms.TreeView();
            this.b_treeview = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(144, 12);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(245, 266);
            this.treeView.TabIndex = 1;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // b_treeview
            // 
            this.b_treeview.Location = new System.Drawing.Point(12, 12);
            this.b_treeview.Name = "b_treeview";
            this.b_treeview.Size = new System.Drawing.Size(121, 56);
            this.b_treeview.TabIndex = 2;
            this.b_treeview.Text = "TreeView";
            this.b_treeview.UseVisualStyleBackColor = true;
            this.b_treeview.Click += new System.EventHandler(this.b_treeview_Click);
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(395, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(239, 266);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 290);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.b_treeview);
            this.Controls.Add(this.treeView);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Button b_treeview;
        private System.Windows.Forms.ListView listView1;
    }
}

