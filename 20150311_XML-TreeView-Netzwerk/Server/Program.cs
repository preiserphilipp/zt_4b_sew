﻿#define LOG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Xml;

namespace Server
{
    class Program
    {
        static TcpListener listener;
        const int LIMIT = 5; //5 concurrent clients

        public static void Main()
        {
            listener = new TcpListener(2055);
            listener.Start();
#if LOG
            Console.WriteLine("Server mounted, listening to port 2055");
#endif
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }
        static XmlDocument xd = new XmlDocument();
        static private void MakeXML(string path, XmlNode current)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo fi = new FileInfo(path);

            foreach (var item in di.GetDirectories())
            {
                XmlNode neu = xd.CreateNode("element", "DIR", "");
                current.AppendChild(neu);

                XmlAttribute name = xd.CreateAttribute("name");
                name.Value = item.Name;
                neu.Attributes.Append(name);

                MakeXML(item.FullName, neu);
            }
            foreach (var item in di.GetFiles())
            {
                XmlNode neu = xd.CreateNode("element", "FILE", "");
                current.AppendChild(neu);

                XmlAttribute name = xd.CreateAttribute("name");
                name.Value = item.Name;
                neu.Attributes.Append(name);

                XmlAttribute size = xd.CreateAttribute("size");
                string seis = item.Length.ToString();
                size.Value = seis;
                neu.Attributes.Append(size);

                //MakeXML(item.FullName, neu);
            }
        }
        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                //soc.SetSocketOption(SocketOptionLevel.Socket,
                //        SocketOptionName.ReceiveTimeout,10000);
#if LOG
                Console.WriteLine("Connected: {0}", 
                                         soc.RemoteEndPoint);
#endif
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true; // enable automatic flushing
                    xd.LoadXml("<DIR name='Desktop'/>");
                    XmlNode root = xd.SelectSingleNode("//DIR");

                    MakeXML(@"C:\Users\Philipp\Desktop", root);
                    sw.WriteLine(xd.InnerXml);

                    s.Close();
                }
                catch (Exception e)
                {
#if LOG
                    Console.WriteLine(e.Message);
#endif
                }
#if LOG
                Console.WriteLine("Disconnected: {0}", soc.RemoteEndPoint);
#endif
                soc.Close();
            }
        }
    }
}
