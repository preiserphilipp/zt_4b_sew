﻿#define LOG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Xml;

namespace _20150423_MeineHTTPFactoryMethod
{
    class Program
    {
        static TcpListener listener;
        const int LIMIT = 5; //5 concurrent clients

        public static void Main()
        {
            listener = new TcpListener(4711);
            listener.Start();
#if LOG
            Console.WriteLine("Server mounted, listening to port 4711");
#endif
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }
        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                //soc.SetSocketOption(SocketOptionLevel.Socket,
                //        SocketOptionName.ReceiveTimeout,10000);
#if LOG
                Console.WriteLine("Connected: {0}", 
                                         soc.RemoteEndPoint);
#endif
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true; // enable automatic flushing
                    string htm, txt;
                    string file = sr.ReadLine();
                    string[] array = file.Split(' ');
                    
                    //generierung eines .txt und eines .htm Strings zu Überprüfung
                    if (array[1].EndsWith(".htm") || array[1].EndsWith(".html"))
                    {
                        htm = array[1];
                        txt = htm.Split('.')[0] + ".txt";
                    }
                    else if (array[1].EndsWith(".txt"))
                    {
                        txt = array[1];
                        htm = txt.Split('.')[0] + ".htm";
                    }
                    else 
                    {
                        txt = array[1] + ".txt";
                        htm = array[1] + ".htm";
                    }
                    //Sendelogik
                    if (array[1] == "/")
                    {
                        Response response = new defaultResponse();
                    }
                    if(array[1] != "/")
                    {
                        if (File.Exists(@"C:\xampp\htdocs\" + txt))
                        {
                            sw.WriteLine(header + File.ReadAllText(@"C:\xampp\htdocs" + txt));
                        }
                        else if (File.Exists(@"C:\xampp\htdocs\" + htm))
                        {
                            Console.WriteLine("Failed to find " + txt);
                            Console.WriteLine("Request for " + htm);
                            sw.WriteLine(header + File.ReadAllText(@"C:\xampp\htdocs" + htm));
                        }
                        else
                        {
                            header = "HTTP/1.1 404 Not Found\r\n";
                            header += "Date: Tue, 15 Nov 1994 08:12:31 GMT \r\n";
                            header += "Server: Apache/1.3.27 (Unix) (Red-Hat/Linux) \r\n";
                            header += "Last-Modified: Tue, 15 Nov 1994 12:45:26 GMT \r\n";
                            header += "\r\n";
                            if (File.Exists(@"C:\xampp\htdocs\_index.html"))
                            {
                                Console.WriteLine("404: File not found");
                                sw.WriteLine(header + File.ReadAllText(@"C:\xampp\htdocs\_notFound.html"));
                            }
                            else
                            {
                                sw.WriteLine("404: File not found");
                                Console.WriteLine("404: File not found");
                            }
                        }
                    }
                    s.Close();
                }
                catch (Exception e)
                {
#if LOG
                    Console.WriteLine(e.Message);
#endif
                }
#if LOG
                Console.WriteLine("Disconnected: {0}", soc.RemoteEndPoint);
#endif
                soc.Close();
            }
        }
    }
}
