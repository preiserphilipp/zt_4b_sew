﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Xml;

namespace _20150423_MeineHTTPFactoryMethod
{
    public class Response
    {
        //HTTP-header
        protected string header = "HTTP/1.1 200 OK \r\nDate: Tue, 15 Nov 1994 08:12:31 GMT \r\nServer: Apache/1.3.27 (Unix) (Red-Hat/Linux) \r\nLast-Modified: Tue, 15 Nov 1994 12:45:26 GMT \r\n";
        protected string endswith;
        protected StreamWriter sw = new StreamWriter(s);
        public string getHeader();
        public void respond();
    }

    public class defaultResponse : Response
    {
        public void respond()
        {
            sw.WriteLine(header + File.ReadAllText(@"C:\xampp\htdocs\_index.html"));
        }
    }      
    public class textResponse:Response
    {
        public string getHeader(){
            if (endswith == ".htm" || endswith == ".html")
                header += "ContentType: text/html \r\n";
            else if (endswith == ".txt")
                header += "ContentType: text/plain \r\n";
            return header;
        }
        public void respond()
        {
            sw.WriteLine(header + File.ReadAllText(@"C:\xampp\htdocs\_index.html"));
        }
    }
    public class errorResponse:Response
    {
        public string getHeader()
        {
            header = "HTTP/1.1 404 Not Found\r\n";
            header += "Date: Tue, 15 Nov 1994 08:12:31 GMT \r\n";
            header += "Server: Apache/1.3.27 (Unix) (Red-Hat/Linux) \r\n";
            header += "Last-Modified: Tue, 15 Nov 1994 12:45:26 GMT \r\n";
            header += "\r\n";
            return header;
        }
    }
    public class binaryResponse:Response
    {
        public string getHeader()
        {
            if (endswith == ".png")
                header += "ContentType: image/png \r\n";
            else if (endswith == ".gif")
                header += "ContentType: image/gif \r\n";
            return header;
        }
    }
}
